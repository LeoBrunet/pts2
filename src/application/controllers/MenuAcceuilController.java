package application.controllers;

import application.models.Contexte;
import application.models.Niveau;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class MenuAcceuilController extends Controller {

	@FXML
	private BarreJoueurController barreJoueurController;

	@FXML
	private AnchorPane jouerBouton;

	@FXML
	private AnchorPane magasinBouton;

	@FXML
	private AnchorPane classementBouton;

	@FXML
	private AnchorPane parametreBouton;

	@FXML
	private AnchorPane deconnexionBouton;

	@FXML
	private AnchorPane background;

	@FXML
	private ImageView decoButton;

	@FXML
	private ImageView trophee;

	@FXML
	private ImageView shop;

	ScaleTransition animTrophee;

	@FXML
	public void initialize() {

		jouerBouton.getStyleClass().add("clickableButton");
		magasinBouton.getStyleClass().add("clickableButton");
		classementBouton.getStyleClass().add("clickableButton");
		parametreBouton.getStyleClass().add("clickableButton");
		decoButton.getStyleClass().add("clickableButton");

		shop.setTranslateX(60);

		animTrophee = new ScaleTransition(Duration.millis(300), trophee);
		animTrophee.setToX(1.3);
		animTrophee.setToY(1.3);
		animTrophee.setAutoReverse(true);
		animTrophee.setCycleCount(Timeline.INDEFINITE);

	}

	@FXML
	public void enterClassement() {
		animTrophee.play();
	}

	@FXML
	public void exitClassement() {
		animTrophee.stop();
		trophee.setScaleX(1);
		trophee.setScaleY(1);
	}

	@FXML
	public void enterShop() {
		ScaleTransition anim = new ScaleTransition(Duration.millis(200), shop);
		anim.setToX(1.4);
		anim.setToY(1.4);
		anim.play();
	}

	@FXML
	public void exitShop() {
		ScaleTransition anim = new ScaleTransition(Duration.millis(200), shop);
		anim.setToX(1);
		anim.setToY(1);
		anim.play();
	}

	@FXML
	public void clickJouer() {
		if (!isEnChargement()) {
			setEnChargement(true);
			getMain().afficherChoixModeJeu();
		}
	}

	@FXML
	public void clickClassement() {
		if (!isEnChargement()) {
			setEnChargement(true);
			getMain().afficherClassement(false, Contexte.DIURNE, Niveau.STARTER);
		}

	}

	@FXML
	public void clickParametres() {
		if (!isEnChargement()) {
			setEnChargement(true);
			getMain().afficherParametres("accueil");
		}
	}

	@FXML
	public void clickDeco() {
		if (!isEnChargement()) {
			setEnChargement(true);
			getMain().afficherMenuChoixProfil();
		}
	}

	public BarreJoueurController getBarreJoueurController() {
		return barreJoueurController;
	}
}
